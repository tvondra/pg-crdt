MODULE_big = crdt
OBJS = src/crdt_counter.o

EXTENSION = crdt
DATA = sql/crdt--0.1.0.sql
MODULES = crdt

TESTS        = $(wildcard test/sql/*.sql)
REGRESS      = $(patsubst test/sql/%.sql,%,$(TESTS))
REGRESS_OPTS = --inputdir=test --load-language=plpgsql

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

all: crdt.so

superloglog_counter.so: $(OBJS)

%.o : src/%.c
