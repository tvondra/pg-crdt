#ifndef CRDT_COUNTER_H
#define CRDT_COUNTER_H

#include "access/htup.h"

#define CRDT_COUNTER_MAGIC		0x53F43EDA
#define CRDT_COUNTER_VERSION	1

/* represents ID of the node (within BDR) */
typedef struct node_id_t
{
	uint64	system_identifier;	/* GetSystemIdentifier() */
	uint32	timeline_id;		/* node timeline */
	uint32	database_oid;		/* node database */
} node_id_t;

#define NODES_EQUAL(id1, id2) \
	((id1.system_identifier == id2.system_identifier) && \
	 (id1.timeline_id == id2.timeline_id) && \
	 (id1.database_oid == id2.database_oid))

/* combination of node ID + counter value */
typedef struct crdt_node_counter_t
{
	node_id_t	node_id;
	int64		value;
} crdt_node_counter_t;

/* an array of counter values */
typedef struct crdt_counter_t
{
	uint32	magic;					/* magic constant for verification */
	uint32	version;				/* data type versioning */
	int		nnodes;					/* number of nodes in the counter */
	crdt_node_counter_t	nodes[1];	/* array of per-node counters */
} crdt_counter_t;

#endif
