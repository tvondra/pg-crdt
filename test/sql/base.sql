\set ECHO none
\i sql/crdt--0.1.0.sql
\set ECHO all

select (((NULL::crdt_counter + 1000) + 1000) +  ((NULL::crdt_counter + 1220000)))::bigint;
  
select (((NULL::crdt_counter + 1000) + 1000) || ((NULL::crdt_counter + 1220000)))::bigint;
