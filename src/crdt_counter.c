#include "postgres.h"

#include "access/heapam.h"
#include "access/htup_details.h"
#include "access/xlog.h"
#include "fmgr.h"
#include "miscadmin.h"
#include "utils/bytea.h"
#include "utils/builtins.h"
#include "utils/rel.h"

#include "crdt_counter.h"

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(crdt_counter_in);
PG_FUNCTION_INFO_V1(crdt_counter_out);
PG_FUNCTION_INFO_V1(crdt_counter_add);
PG_FUNCTION_INFO_V1(crdt_counter_value);
PG_FUNCTION_INFO_V1(crdt_counter_text);
PG_FUNCTION_INFO_V1(crdt_counter_merge);
PG_FUNCTION_INFO_V1(crdt_counter_combine);
PG_FUNCTION_INFO_V1(crdt_conflict_handler);

static HeapTuple crdt_conflict_handler_internal(HeapTuple local, HeapTuple remote,
										 text *commandtag, Oid relation, Oid event);

static node_id_t local_node_id(void);
static bool datum_is_crdt_counter(Datum value, bool isnull);

Datum
crdt_counter_in(PG_FUNCTION_ARGS)
{
	char *ptr = PG_GETARG_CSTRING(0);

	return DirectFunctionCall1(byteain, PointerGetDatum(ptr));
}

Datum
crdt_counter_out(PG_FUNCTION_ARGS)
{
	bytea *ptr = PG_GETARG_BYTEA_P(0);

	return DirectFunctionCall1(byteaout, PointerGetDatum(ptr));
}

Datum
crdt_counter_text(PG_FUNCTION_ARGS)
{
	int i;
	bytea   *ptr     = PG_GETARG_BYTEA_P(0);
	crdt_counter_t  *counter = (crdt_counter_t*)VARDATA(ptr);

	StringInfoData buf;
	initStringInfo(&buf);

	appendStringInfoString(&buf, "CRDT:COUNTER");

	for (i = 0; i < counter->nnodes; i++)
		appendStringInfo(&buf, " (%ld,%d,%d,%ld)",
						 counter->nodes[i].node_id.system_identifier,
						 counter->nodes[i].node_id.timeline_id,
						 counter->nodes[i].node_id.database_oid,
						 counter->nodes[i].value);

    PG_RETURN_TEXT_P(cstring_to_text(buf.data));
}

Datum
crdt_counter_add(PG_FUNCTION_ARGS)
{
	int			len;
	bytea	   *result;
	int64		inc     = PG_GETARG_INT64(1);
	node_id_t	node_id = local_node_id();

	crdt_counter_t *counter;

	if (PG_ARGISNULL(0))
	{
		/* we need space for one node */
		counter = palloc0(offsetof(crdt_counter_t,nodes) +  sizeof(crdt_node_counter_t));
		counter->magic = CRDT_COUNTER_MAGIC;
		counter->version = CRDT_COUNTER_VERSION;
		counter->nnodes = 1;
		counter->nodes[0].node_id = node_id;
		counter->nodes[0].value = inc;
	}
	else
	{
		int			i;
		bool		found = false;

		bytea	   *ptr     = PG_GETARG_BYTEA_P(0);

		Assert(ptr != NULL);

		counter = (crdt_counter_t*)VARDATA_ANY(ptr);

		Assert(counter->magic == CRDT_COUNTER_MAGIC);
		Assert(counter->version == CRDT_COUNTER_VERSION);

		len = offsetof(crdt_counter_t,nodes) +
			  counter->nnodes * sizeof(crdt_node_counter_t);

		Assert(VARSIZE_ANY_EXHDR(ptr) == len);

		for (i = 0; i < counter->nnodes; i++)
		{
			if (NODES_EQUAL(counter->nodes[i].node_id, node_id))
			{
				counter->nodes[i].value += inc;
				found = true;
				break;
			}
		}

		if (! found)
		{
			crdt_counter_t * counter_new;

			/* current length */
			len = offsetof(crdt_counter_t,nodes) +
				  counter->nnodes * sizeof(crdt_node_counter_t);

			/* space with the new node */
			counter_new = palloc0(len + sizeof(crdt_node_counter_t));

			memcpy(counter_new, counter, len);

			/* forget about the old counter */
			counter = counter_new;

			counter->nnodes += 1;

			counter->nodes[counter->nnodes-1].node_id = node_id;
			counter->nodes[counter->nnodes-1].value = inc;
		}
	}

	/* current length (with the new node, if needed) */
	len = offsetof(crdt_counter_t,nodes) +
		  counter->nnodes * sizeof(crdt_node_counter_t);

	/* allocate space for the bytea-encoded counter */
	result = palloc0(len + VARHDRSZ);

	SET_VARSIZE(result, len + VARHDRSZ);
	memcpy(VARDATA(result), counter, len);

	PG_RETURN_BYTEA_P(result);
}

Datum
crdt_counter_value(PG_FUNCTION_ARGS)
{
	int			i, len;
	bytea	   *ptr = PG_GETARG_BYTEA_P(0);
	int64		result = 0;

	crdt_counter_t * counter = (crdt_counter_t*)VARDATA_ANY(ptr);

	Assert(counter->magic == CRDT_COUNTER_MAGIC);
	Assert(counter->version == CRDT_COUNTER_VERSION);

	len = offsetof(crdt_counter_t,nodes) +
		  counter->nnodes * sizeof(crdt_node_counter_t);

	Assert(VARSIZE_ANY_EXHDR(ptr) == len);

	for (i = 0; i < counter->nnodes; i++)
		result += counter->nodes[i].value;

	PG_RETURN_INT64(result);
}

static node_id_t
local_node_id()
{
	node_id_t id;

	id.system_identifier = GetSystemIdentifier();
	id.timeline_id = ThisTimeLineID;
	id.database_oid = MyDatabaseId;

	return id;
}

#define MAX(a,b)  (((a) > (b)) ? (a) : (b))

Datum
crdt_counter_merge(PG_FUNCTION_ARGS)
{
	int		i, len;
	bytea  *ptra, *ptrb, *result;
	crdt_counter_t *countera, *counterb, *counter;

	/* if both parameters are NULL, return NULL */
	if (PG_ARGISNULL(0) && PG_ARGISNULL(1))
		PG_RETURN_NULL();
	else if (PG_ARGISNULL(0))
		PG_RETURN_BYTEA_P(PG_GETARG_BYTEA_P(1));
	else if (PG_ARGISNULL(1))
		PG_RETURN_BYTEA_P(PG_GETARG_BYTEA_P(0));

	/* if both are not-NULL, we need to actually merge them */

	ptra = PG_GETARG_BYTEA_P(0);
	ptrb = PG_GETARG_BYTEA_P(01);

	countera = (crdt_counter_t*)VARDATA_ANY(ptra);
	counterb = (crdt_counter_t*)VARDATA_ANY(ptrb);

	Assert(countera->magic == CRDT_COUNTER_MAGIC);
	Assert(counterb->magic == CRDT_COUNTER_MAGIC);

	Assert(countera->version == CRDT_COUNTER_VERSION);
	Assert(counterb->version == CRDT_COUNTER_VERSION);

	/* allocate the largest possible counter (as if there was no overlap) */
	counter = palloc0(offsetof(crdt_counter_t,nodes) +
					  (countera->nnodes + counterb->nnodes) * sizeof(crdt_node_counter_t));

	/* now copy one of the counters into the result */
	memcpy(counter, countera, VARSIZE_ANY_EXHDR(ptra));

	/* now walk through the other counter and merge it into the result */
	for (i = 0; i < counterb->nnodes; i++)
	{
		int		j;
		bool	found = false;

		/*
		 * we'll search in the original counter, but add the data to the
		 * new one (it's just a copy, so sorted the same way)
		 */
		for (j = 0; j < countera->nnodes; j++)
		{
			if (NODES_EQUAL(counterb->nodes[i].node_id, countera->nodes[j].node_id))
			{
				counter->nodes[j].value = MAX(counterb->nodes[i].value,
											  countera->nodes[j].value);
				found = true;
				break;
			}
		}

		/* if not found in the result counter, just add it to the end */
		if (! found)
		{
			counter->nodes[counter->nnodes] = counterb->nodes[i];
			counter->nnodes += 1;
		}
	}

	/* current length (with the new node, if needed) */
	len = offsetof(crdt_counter_t,nodes) +
		  counter->nnodes * sizeof(crdt_node_counter_t);

	/* allocate space for the bytea-encoded counter */
	result = palloc0(len + VARHDRSZ);

	SET_VARSIZE(result, len + VARHDRSZ);
	memcpy(VARDATA(result), counter, len);

	PG_RETURN_BYTEA_P(result);
}

Datum
crdt_counter_combine(PG_FUNCTION_ARGS)
{
	int		i, len;
	bytea  *ptra, *ptrb, *result;
	crdt_counter_t *countera, *counterb, *counter;

	/* if both parameters are NULL, return NULL */
	if (PG_ARGISNULL(0) && PG_ARGISNULL(1))
		PG_RETURN_NULL();
	else if (PG_ARGISNULL(0))
		PG_RETURN_BYTEA_P(PG_GETARG_BYTEA_P(1));
	else if (PG_ARGISNULL(1))
		PG_RETURN_BYTEA_P(PG_GETARG_BYTEA_P(0));

	/* if both are not-NULL, we need to actually merge them */

	ptra = PG_GETARG_BYTEA_P(0);
	ptrb = PG_GETARG_BYTEA_P(01);

	countera = (crdt_counter_t*)VARDATA_ANY(ptra);
	counterb = (crdt_counter_t*)VARDATA_ANY(ptrb);

	Assert(countera->magic == CRDT_COUNTER_MAGIC);
	Assert(counterb->magic == CRDT_COUNTER_MAGIC);

	/* allocate the largest possible counter (as if there was no overlap) */
	counter = palloc0(offsetof(crdt_counter_t,nodes) +
					  (countera->nnodes + counterb->nnodes) * sizeof(crdt_node_counter_t));

	/* now copy one of the counters into the result */
	memcpy(counter, countera, VARSIZE_ANY_EXHDR(ptra));

	/* now walk through the other counter and merge it into the result */
	for (i = 0; i < counterb->nnodes; i++)
	{
		int		j;
		bool	found = false;

		/*
		 * we'll search in the original counter, but add the data to the
		 * new one (it's just a copy, so sorted the same way)
		 */
		for (j = 0; j < countera->nnodes; j++)
		{
			if (NODES_EQUAL(counterb->nodes[i].node_id, countera->nodes[j].node_id))
			{
				counter->nodes[j].value = counterb->nodes[i].value +
										  countera->nodes[j].value;
				found = true;
				break;
			}
		}

		/* if not found in the result counter, just add it to the end */
		if (! found)
		{
			counter->nodes[counter->nnodes] = counterb->nodes[i];
			counter->nnodes += 1;
		}
	}

	/* current length (with the new node, if needed) */
	len = offsetof(crdt_counter_t,nodes) +
		  counter->nnodes * sizeof(crdt_node_counter_t);

	/* allocate space for the bytea-encoded counter */
	result = palloc0(len + VARHDRSZ);

	SET_VARSIZE(result, len + VARHDRSZ);
	memcpy(VARDATA(result), counter, len);

	PG_RETURN_BYTEA_P(result);
}

static bool
datum_is_crdt_counter(Datum value, bool isnull)
{
	int		len;
	bytea  *ptr;
	crdt_counter_t *counter;

	/* we do consider NULL to be a CRDT value */
	if (isnull)
		return true;

	ptr = DatumGetByteaP(value);
	counter = (crdt_counter_t*)VARDATA_ANY(ptr);

	if (counter->magic != CRDT_COUNTER_MAGIC)
		return false;

	if (counter->version != CRDT_COUNTER_VERSION)
		return false;

	len = offsetof(crdt_counter_t,nodes) +
		  counter->nnodes * sizeof(crdt_node_counter_t);

	return (VARSIZE_ANY_EXHDR(ptr) == len);
}

/*
 * handle conflicts for CRDT types
 *
 * - local tuple
 * - remote tuple
 * - command tag
 * - OID of the relation
 * - OID of the event (conflict type)
 */
static HeapTuple
crdt_conflict_handler_internal(HeapTuple local, HeapTuple remote, text *commandtag,
					  Oid relation_oid, Oid event_oid)
{
	int			i;
	HeapTuple	htup;
	Relation	rel = relation_open(relation_oid, NoLock);
	TupleDesc	tdesc = RelationGetDescr(rel);

	/* the resulting tuple */
	Datum	   *values = (Datum*)palloc0(sizeof(Datum) * tdesc->natts);
	bool	   *isnull = (bool *)palloc0(sizeof(bool)  * tdesc->natts);

	/* combine the two tuples */
	for (i = 0; i < tdesc->natts; i++)
	{
		AttrNumber attnum = tdesc->attrs[i]->attnum;
		Datum value_local, value_remote;
		bool  isnull_local, isnull_remote;

		/*
		 * We don't know the OIDs of the CRDT data types, because those are
		 * dynamic (assigned when creating extension), so we have to detect
		 * them differently.
		 *
		 * CRDT data types are encoded in a varlena, so typlen has to be -1,
		 * and we'll also check the magic constants. It's not perfect, as
		 * there really might be a random bytea value with the same magic
		 * value.
		 *
		 * We might lookup the OID by type name, but that's not perfectly
		 * reliable as people might have created custom data types with the
		 * same name (but maybe that'd be a conflict with this extension?).
		 */
		if (tdesc->attrs[i]->attlen != -1)
		{
			/* XXX too bad we can't decide which of the two tuples is newer */
			values[i] = heap_getattr(local, attnum, tdesc, &isnull[i]);
			continue;
		}

		/* now make sure both varlena values are non-NULL */
		value_local  = heap_getattr(local,  attnum, tdesc, &isnull_local);
		value_remote = heap_getattr(remote, attnum, tdesc, &isnull_remote);

		/*
		 * check that both values are actually CRDT counters - if either
		 * of them is not, just use the local value (we can only get one
		 * NULL value here, not two)
		 *
		 * If it's not a CRDT data type, we'll simply use the local value.
		 *
		 * XXX again, if we could somehow decide which tuple is newer,
		 *     that'd be nice (or perhaps if we could perform partial
		 *     merge and let some other handler to take care of the
		 *     remaining attributes)
		 */
		if (! (datum_is_crdt_counter(value_local,  isnull_local) &&
			   datum_is_crdt_counter(value_remote, isnull_remote)))
		{
			values[i] = value_local;
			isnull[i] = isnull_local;
			continue;
		}

		/*
		 * ok, so we know there's at least one non-NULL value and that
		 * we can merge the non-NULL values using crdt_counter_merge
		 *
		 * we'll deal with the NULL cases first, so that we can then
		 * use DirectFunctionCall2 (so that we don't need to mess with
		 * FmgrInfo to send NULLs)
		 */

		/* if both values are NULL, we just use NULL */
		if (isnull_local && isnull_remote)
		{
			isnull[i] = true;
			continue;
		}
		else if (isnull_local)
		{
			values[i] = value_remote;
			continue;
		}
		else if (isnull_remote)
		{
			values[i] = value_local;
			continue;
		}

		/* both values are non-NULL, so just call the merge */
		values[i] = DirectFunctionCall2(crdt_counter_merge,
										value_local, value_remote);
	}

	/* cool, let's build the resulting tuple */
	htup = heap_form_tuple(tdesc, values, isnull);

	relation_close(rel, NoLock);

	return htup;
}

Datum
crdt_conflict_handler(PG_FUNCTION_ARGS)
{
	HeapTuple	result;
	HeapTupleHeader	local = PG_GETARG_HEAPTUPLEHEADER(0);
	HeapTupleHeader	remote = PG_GETARG_HEAPTUPLEHEADER(1);
	text	   *commandtag = PG_GETARG_TEXT_P(2);
	Oid			relation_oid = PG_GETARG_OID(3);
	Oid			event_oid = PG_GETARG_OID(4);

	HeapTupleData	local_tuple, remote_tuple;

	local_tuple.t_data = local;
	local_tuple.t_len =  HeapTupleHeaderGetDatumLength(local);

	remote_tuple.t_data = remote;
	remote_tuple.t_len =  HeapTupleHeaderGetDatumLength(remote);

	result = crdt_conflict_handler_internal(&local_tuple, &remote_tuple,
											commandtag,
											relation_oid, event_oid);

	PG_RETURN_POINTER(result);
}
