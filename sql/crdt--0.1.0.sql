CREATE TYPE crdt_counter;

CREATE OR REPLACE FUNCTION crdt_counter_in(cstring)
RETURNS crdt_counter
AS 'crdt', 'crdt_counter_in'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION crdt_counter_out(crdt_counter)
RETURNS CSTRING
AS 'crdt', 'crdt_counter_out'
LANGUAGE C IMMUTABLE STRICT;

CREATE TYPE crdt_counter (
    INPUT   = crdt_counter_in,
    OUTPUT  = crdt_counter_out
);


CREATE OR REPLACE FUNCTION crdt_counter_add(crdt_counter, BIGINT)
RETURNS crdt_counter
AS 'crdt', 'crdt_counter_add'
LANGUAGE C IMMUTABLE;

CREATE OR REPLACE FUNCTION crdt_counter_value(crdt_counter)
RETURNS BIGINT
AS 'crdt', 'crdt_counter_value'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION crdt_counter_text(crdt_counter)
RETURNS TEXT
AS 'crdt', 'crdt_counter_text'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION crdt_counter_combine(crdt_counter, crdt_counter)
RETURNS crdt_counter
AS 'crdt', 'crdt_counter_combine'
LANGUAGE C IMMUTABLE;

CREATE OR REPLACE FUNCTION crdt_counter_merge(crdt_counter, crdt_counter)
RETURNS crdt_counter
AS 'crdt', 'crdt_counter_merge'
LANGUAGE C IMMUTABLE;

CREATE CAST (crdt_counter AS text)
    WITH FUNCTION crdt_counter_text (crdt_counter);

CREATE CAST (crdt_counter AS bigint)
    WITH FUNCTION crdt_counter_value (crdt_counter);

CREATE OPERATOR + (
    PROCEDURE = crdt_counter_add,
    LEFTARG = crdt_counter,
    RIGHTARG = bigint
);

CREATE OPERATOR + (
    PROCEDURE = crdt_counter_combine,
    LEFTARG = crdt_counter,
    RIGHTARG = crdt_counter
);

CREATE OPERATOR || (
    PROCEDURE = crdt_counter_merge,
    LEFTARG = crdt_counter,
    RIGHTARG = crdt_counter
);

CREATE OR REPLACE FUNCTION crdt_conflict_handler(IN local_row INTERNAL, IN remote_row INTERNAL,
												 IN cmdtag TEXT, IN relation_oid OID, IN conflict_type OID,
												 OUT result_row INTERNAL, OUT handler_action OID)
RETURNS RECORD
AS 'crdt', 'crdt_conflict_handler'
LANGUAGE C IMMUTABLE;

CREATE TABLE test (
    id      INT PRIMARY KEY,
    counter CRDT_COUNTER
);

CREATE OR REPLACE FUNCTION crdt_test_handler(IN local_row TEST, IN remote_row TEST, IN cmdtag TEXT, IN relation REGCLASS,
                                             IN conflict_type bdr.bdr_conflict_type,
                                             OUT result_row TEST, OUT conflict_action bdr.bdr_conflict_handler_action)
RETURNS RECORD
AS 'crdt', 'crdt_conflict_handler'
LANGUAGE C IMMUTABLE;

SELECT bdr.bdr_create_conflict_handler('test'::regclass, 'a', 'crdt_test_handler'::regproc, 'insert_insert');
SELECT bdr.bdr_create_conflict_handler('test'::regclass, 'b', 'crdt_test_handler'::regproc, 'update_update');
